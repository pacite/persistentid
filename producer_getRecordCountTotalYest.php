<?php

$host = 'redshift-cluster-ds2.cncolz3pcw1i.us-west-2.redshift.amazonaws.com';
$db = 'production';

$start = date('Y-m-d 00:00:00',strtotime("-1 days"));
$end = date('Y-m-d 23:59:59.997',strtotime("-1 days"));

$con = new PDO(
    'pgsql:dbname='.$db.';host='.$host.';port=5439',
    'ro_user',
    'AbC789XyZ_!'
    );

$q = "SELECT pixel_id, COUNT(user_id) AS num_records FROM personlink_impression WHERE last_seen BETWEEN '$start' AND '$end' GROUP BY pixel_id ORDER BY pixel_id";
$s = $con->prepare($q);
$s->execute();
$ret = $s->fetchAll();

echo "PRODUCER\n";
echo "Pixel_ID\t|\tRecords\n";
echo "-------------------------------\n";

foreach ($ret as $row) {
    echo str_pad($row['pixel_id'],8,' ') . "\t|\t" . number_format($row['num_records']) . "\n";
}
