<?php

$start='';$end='';
if (isset($argv[1])){ $start = $argv[1];}
if (isset($argv[2])){ $end = $argv[2];}

if (($start == "") || ($end == "")) {
    echo "\nStart and End times are required.\n";
    echo "Example:\n";
    echo "$argv[0] \"2021-01-04 00:00:00\" \"2021-01-04 23:59:59.997\"\n\n";
    die;
}

// 1st get the indexes of our consumers
require 'vendor/autoload.php';
use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'profile' => 'default',
    'region'  => 'us-west-2',
    'version' => 'latest'
));

$iterator = $client->getIterator('Scan', array(
    'TableName' => 'personlink_consumer_v2',
    'FilterExpression' => 'active = :a',
    'ProjectionExpression' => '#IN,#CID,#CNAME',
    'ExpressionAttributeNames' => array(
        '#IN' => 'index',
        '#CID' => 'consumer_id',
        '#CNAME' => 'consumer_name'
    ),
    'ExpressionAttributeValues' => array(
        ':a' => array(
            'BOOL' => True
        )
    )
));

// Make our RedShift Connection
$host = 'redshift-cluster-ds2.cncolz3pcw1i.us-west-2.redshift.amazonaws.com';
$db = 'production';

$total_records = 0;
$records = array();

$con = new PDO(
    'pgsql:dbname='.$db.';host='.$host.';port=5439',
    'ro_user',
    'AbC789XyZ_!'
    );

// Loop through our Dynamo results
foreach ($iterator as $item) {
    $idx = $item['index']['N'];
    $cid = $item['consumer_id']['S'];
    $cn = $item['consumer_name']['S'];
    echo "Counting records for '$cn' ($cid) between '$start' and '$end' ...\n";

    $q = "SELECT COUNT(1) AS num_records FROM personlink_impression WHERE last_seen BETWEEN '$start' AND '$end' AND SUBSTRING(REVERSE(consumers), $idx, 1) = 1";
    $s = $con->prepare($q);
    $s->execute();
    $ret = $s->fetch();

    $records[$cid] = $ret['num_records'];
    $total_records += $ret['num_records'];

}

echo "\nCONSUMERS\n";

foreach ($records as $key => $value) {
    print $key . ": " . number_format($value) . "\n";
}

echo "Number of Records: ". number_format($total_records) ." \n";
