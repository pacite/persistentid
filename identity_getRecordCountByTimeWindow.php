<?php

$start='';$end='';
if (isset($argv[1])){ $start = $argv[1];}
if (isset($argv[2])){ $end = $argv[2];}

if (($start == "") || ($end == "")) {
    echo "\nStart and End times are required.\n";
    echo "Example:\n";
    echo "$argv[0] \"2021-01-04 00:00:00\" \"2021-01-04 23:59:59.997\"\n\n";
    die;
}

// 1st get the indexes of our providers
require 'vendor/autoload.php';
use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'profile' => 'default',
    'region'  => 'us-west-2',
    'version' => 'latest'
));

$iterator = $client->getIterator('Scan', array(
    'TableName' => 'personlink_identity_provider_v2',
    'FilterExpression' => 'active = :a',
    'ProjectionExpression' => '#IN,#PID,#PNAME',
    'ExpressionAttributeNames' => array(
        '#IN' => 'index',
        '#PID' => 'provider_id',
        '#PNAME' => 'provider_name'
    ),
    'ExpressionAttributeValues' => array(
        ':a' => array(
            'BOOL' => True
        )
    )
));

// Make our RedShift Connection
$host = 'redshift-cluster-ds2.cncolz3pcw1i.us-west-2.redshift.amazonaws.com';
$db = 'production';

$total_records = 0;
$records = array();

$con = new PDO(
    'pgsql:dbname='.$db.';host='.$host.';port=5439',
    'ro_user',
    'AbC789XyZ_!'
    );

// Loop through our Dynamo results
foreach ($iterator as $item) {
    $idx = $item['index']['N'];
    $pid = $item['provider_id']['S'];
    $pn = $item['provider_name']['S'];
    echo "Counting records for '$pn' ($pid) between '$start' and '$end' ...\n";

    $q = "SELECT COUNT(1) AS num_records FROM personlink_impression WHERE last_seen BETWEEN '$start' AND '$end' AND SUBSTRING(REVERSE(identifiers), $idx, 1) = 1";
    $s = $con->prepare($q);
    $s->execute();
    $ret = $s->fetch();

    $records[$pid] = $ret['num_records'];
    $total_records += $ret['num_records'];

}

echo "\nIdentity Providersn\n";

foreach ($records as $key => $value) {
    print $key . ": " . number_format($value) . "\n";
}

echo "\nNumber of Records: ". number_format($total_records) ." \n";
