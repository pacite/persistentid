<?php

require 'vendor/autoload.php';

use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'profile' => 'default',
    'region'  => 'us-west-2',
    'version' => 'latest'
));


/* List DynamoDB Tables */
/*
$result = $client->listTables();

// TableNames contains an array of table names
foreach ($result['TableNames'] as $tableName) {
    echo $tableName . "\n";
}
 */

$iterator = $client->getIterator('Scan', array(
    'TableName' => 'personlink_producer_v2',
    'ProjectionExpression' => '#IN,#PID,#PNAME,#PTYPE',
    'ExpressionAttributeNames' => array(
        '#IN' => 'index',
        '#PID' => 'pixel_id',
        '#PNAME' => 'pixel_name',
        '#PTYPE' => 'producer_type'
    )
));


$recCount = 0;

echo "\n\nPRODUCER\nIndex\t|\tPixel ID\t|\tPixel Name\t\t\t\t|\tProducer Type\n";
echo "-----------------------------------------------------------------------------------------------------\n";

foreach ($iterator as $item) {
    #echo 'consumer_id: ' . $item['consumer_id']['S'] . "\n";
    #var_dump($item);
    $id = $item['index']['N'];
    $pid = $item['pixel_id']['S'];
    $name = $item['pixel_name']['S'];
    $type = $item['producer_type']['S'];

    $id = str_pad($id, 7, ' ');
    $pid = str_pad($pid,8,' ');
    $name = str_pad($name,40,' ');

    echo "$id\t|\t$pid\t|\t$name|\t$type\n";
    $recCount +=1;
}
echo "\nTotal Producers: " . number_format($recCount) . "\n";


?>
