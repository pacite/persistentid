<?php

require 'vendor/autoload.php';

use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'profile' => 'default',
    'region'  => 'us-west-2',
    'version' => 'latest'
));


/* List DynamoDB Tables */
/*
$result = $client->listTables();

// TableNames contains an array of table names
foreach ($result['TableNames'] as $tableName) {
    echo $tableName . "\n";
}
 */

$iterator = $client->getIterator('Scan', array(
    'TableName' => 'personlink_consumer_v2',
    'FilterExpression' => 'active = :a',
    'ProjectionExpression' => '#IN,#CID,#CNAME',
    'ExpressionAttributeNames' => array(
        '#IN' => 'index',
        '#CID' => 'consumer_id',
        '#CNAME' => 'consumer_name'
    ),
    'ExpressionAttributeValues' => array(
        ':a' => array(
            'BOOL' => True
        )
    )
));


$consumerCount = 0;

echo "CONSUMERS\n";
echo "\n\nIndex\t|\tConsumer ID\t|\tConsumer Name\n";
echo "-----------------------------------------------------\n";

foreach ($iterator as $item) {
    #echo 'consumer_id: ' . $item['consumer_id']['S'] . "\n";
    #var_dump($item);
    $in = $item['index']['N'];
    $cid = $item['consumer_id']['S'];
    $cn = $item['consumer_name']['S'];

    echo "$in\t|\t$cid\t|\t$cn\n";
    $consumerCount +=1;
}
echo "\nTotal Active Consumers: " . number_format($consumerCount) . "\n";


?>
