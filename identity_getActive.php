<?php

require 'vendor/autoload.php';

use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'profile' => 'default',
    'region'  => 'us-west-2',
    'version' => 'latest'
));


/* List DynamoDB Tables */
/*
$result = $client->listTables();

// TableNames contains an array of table names
foreach ($result['TableNames'] as $tableName) {
    echo $tableName . "\n";
}
 */

$iterator = $client->getIterator('Scan', array(
    'TableName' => 'personlink_identity_provider_v2',
    'FilterExpression' => 'active = :a',
    'ProjectionExpression' => '#IN,#PID,#PNAME',
    'ExpressionAttributeNames' => array(
        '#IN' => 'index',
        '#PID' => 'provider_id',
        '#PNAME' => 'provider_name'
    ),
    'ExpressionAttributeValues' => array(
        ':a' => array(
            'BOOL' => True
        )
    )
));


$consumerCount = 0;

echo "CONSUMERS\n";
echo "\n\nIndex\t|\tProvider ID\t|\tProvider Name\n";
echo "-----------------------------------------------------\n";

foreach ($iterator as $item) {
    $in = $item['index']['N'];
    $pid = $item['provider_id']['S'];
    $pn = $item['provider_name']['S'];

    echo "$in\t|\t$pid\t|\t$pn\n";
    $consumerCount +=1;
}
echo "\nTotal Active Providers: " . number_format($consumerCount) . "\n";


?>
